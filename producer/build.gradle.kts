plugins {
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-webflux:2.4.0")
    implementation("org.apache.kafka:kafka-clients:2.6.0")
}