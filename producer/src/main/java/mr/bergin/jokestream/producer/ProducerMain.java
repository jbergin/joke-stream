package mr.bergin.jokestream.producer;

import io.vavr.control.Either;
import lombok.Data;
import lombok.val;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.http.HttpStatus;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static org.springframework.boot.WebApplicationType.NONE;

@SpringBootApplication
@EnableBinding(Source.class)
public class ProducerMain {
    private static final String ICANHAZDADJOKE_COM = "https://icanhazdadjoke.com/";

    public static void main(String[] args) {
        new SpringApplicationBuilder(ProducerMain.class).web(NONE).run(args);
    }

    private static Either<HttpStatus, String> retrieveJoke(WebClient webClient) {
        return webClient.get().uri(ICANHAZDADJOKE_COM)
                .header("Accept", "application/json")
                .exchangeToMono(response -> {
                    if (response.statusCode() != HttpStatus.OK) {
                        return Mono.just(Either.<HttpStatus, String>left(response.statusCode()));
                    }
                    return response.bodyToMono(ICanHasDadJoke.class)
                            .map(joke -> Either.<HttpStatus, String>right(joke.getJoke()));
                }).block();
    }

    @InboundChannelAdapter(channel = Source.OUTPUT, poller = @Poller(fixedRate = "60000"))
    public String produce() {
        val webClient = WebClient.create();

        Either<HttpStatus, String> retrievedJoke = retrieveJoke(webClient);

        if (retrievedJoke.isLeft()) {
            val httpStatus = retrievedJoke.getLeft();
            throw new RuntimeException("Failed to retrieve joke with status " + httpStatus);
        }
        return retrievedJoke.get();
    }

    @Data
    public static class ICanHasDadJoke {
        private String joke;
    }
}