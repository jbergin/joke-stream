plugins {
    id("io.freefair.lombok") version "5.3.0" apply false
    id("org.springframework.boot") version "2.4.0" apply false
}

subprojects {
    apply(plugin = "io.freefair.lombok")
    apply(plugin = "java")
    apply(plugin = "org.springframework.boot")

    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        "implementation"("io.vavr:vavr:1.0.0-alpha-3")
        "implementation"("org.springframework.cloud:spring-cloud-starter-stream-kafka:3.0.9.RELEASE")
    }
}