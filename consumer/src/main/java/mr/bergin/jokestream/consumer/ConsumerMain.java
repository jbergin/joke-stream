package mr.bergin.jokestream.consumer;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.config.EnableWebFlux;
import reactor.core.publisher.Mono;

@SpringBootApplication
@EnableBinding(Sink.class)
@RestController
@EnableWebFlux
public class ConsumerMain {

    private volatile String theJoke = "Zee goggles, ZEY DO NOTHING!";

    public static void main(String[] args) {
        new SpringApplicationBuilder(ConsumerMain.class).run(args);
    }

    @StreamListener(Sink.INPUT)
    public void consume(@Payload String joke) {
        theJoke = joke;
    }

    @GetMapping("joke")
    public Mono<String> theJoke() {
        return Mono.just(theJoke);
    }
}