# Joke Stream

A toy project to get to grips with certain technologies. There is one business objective, to provide a joke for each daily stand up.

## How Do I Run It?

gradle bootRun --parallel

Then go to localhost:8080/joke for your latest dad joke, it will (should) change every minute.

##Project aims

To get have a toy implementation that allows devs to cut their teeth on the following:

* Gitlab
* Gradle
* AWS
* Java 11
* Spring, Boot, Cloud Stream
* Kafka
* Kafka Streams
* Vavr
* Lombok
* Avro

## Getting started:

* Fork this repository
* Clone your fork
* Install the latest lombok plugin for intellij

## What about other mentioned patterns?

The following were also mentioned in the talk. There's currently no intent to educate on these topics to
keep the scope of the initial project manageable. This could change over time, however,

### Architectural Patterns
* Event Driven
* Event Sourcing
* Event carried state transfer
* CQRS
* Domain Driven Design
* Alpha Architecture documentation
* Reactive Programming ( https://projectreactor.io/docs/core/release/reference/ )
* Functional Programming

### Technologies (Infra)
* Python
* Bash
* Kubernetes
* Helm

### Technologies (Backend)
* JPA & Spring Data

# Technologies (Frontend)
* Typescript
* React
* Redux